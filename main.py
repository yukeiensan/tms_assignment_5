#!/usr/bin/env python3

import argparse
import math
import csv


# Project functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Draw chart from custom given information")
    parser.add_argument("currencies_file", help="File provided in order to calculate currencies matrix")
    args = parser.parse_args()

    return args


def csv_to_object(filename):
    file = open(filename, "r+")
    csv_raw = list(csv.reader(file))
    csv_data = {
        "header": csv_raw[0],
        "rows": [{
            csv_raw[0][0]: csv_raw[1].pop(0),
            "data":  [float(i) for i in csv_raw[1]]
        }],
    }

    return csv_data


def order_first_row(csv_data):
    for index, cell in enumerate(csv_data["header"]):
        if index >= 2 and cell == csv_data["rows"][0]["iso"]:
            csv_data["header"].insert(1, csv_data["header"].pop(index))
            csv_data["rows"][0]["data"].insert(0, csv_data["rows"][0]["data"].pop(index - 1))

    return csv_data


def prepare_matrix(csv_data):
    for index, currency in enumerate(csv_data["header"]):
        if currency != csv_data["rows"][0]["iso"] and index != 0:
            csv_data["rows"].append({
                csv_data["header"][0]: currency,
                "data": [0] * (len(csv_data["header"]) - 1)
            })

    return order_first_row(csv_data)


def calculate_first_column(csv_data):
    for index, column in enumerate(csv_data["rows"][0]["data"]):
        csv_data["rows"][index]["data"][0] = 1 / column

    return csv_data


def round_down(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier) / multiplier


def calculate_currencies(csv_data):
    for y, row in enumerate(csv_data["rows"]):
        for x, cell in enumerate(row["data"]):
            csv_data["rows"][y]["data"][x] = round(csv_data["rows"][y]["data"][0] * csv_data["rows"][0]["data"][x], 5)

    return csv_data


def print_data(csv_data):
    for index, header in enumerate(csv_data["header"]):
        if index == len(csv_data["header"]) - 1:
            print('\t' + header)
        elif index != 0:
            print('\t' + header, end='')

    for row in csv_data["rows"]:
        print(' ' + row["iso"], end='')
        for column, cell in enumerate(row["data"]):
            if column == len(row["data"]) - 1:
                print('\t' + str(cell))
            else:
                print('\t' + str(cell), end='')


def main():
    args = parsing_arguments()
    csv_data = csv_to_object(args.currencies_file)
    csv_data = prepare_matrix(csv_data)
    csv_data = calculate_first_column(csv_data)
    csv_data = calculate_currencies(csv_data)

    print_data(csv_data)


if __name__ == "__main__":
    main()
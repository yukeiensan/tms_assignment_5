# Requirements
None

# Usage
```usage: currency_pairs.exe [-h] currencies_file

Draw chart from custom given information

positional arguments:
  currencies_file  File provided in order to calculate currencies matrix

optional arguments:
  -h, --help       show this help message and exit`````
